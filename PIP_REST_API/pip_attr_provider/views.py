from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from datetime import datetime 

# Create your views here.
@api_view(['GET'])
def acute_care_call_centre(request):
    if request.method == 'GET':
        try:
            professional_id = request.GET['professional-id']
            encounter_id = request.GET['encounter-id']
            episoode_of_care_id = request.GET['episode-of-care-id']            
        except:
            return HttpResponse(status=404)

        response = {
            "status" : 200,
            "encounter" : {
                "found" : True,
                "end_timestamp" : datetime.now(),
                "episode_of_care" : {
                    "found" : True,
                    "end_timestamp" : datetime.now(),
                    "origin_phonenumber" : "0623347405",
                    "professional_id" : 10
                }
            }
        }
        return Response(response)

# Create your views here.
@api_view(['GET'])
def acute_care_ambulance(request):
    if request.method == 'GET':
        try:
            professional_id = request.GET['professional-id']
            encounter_id = request.GET['encounter-id']
            episoode_of_care_id = request.GET['episode-of-care-id']            
        except:
            return HttpResponse(status=404)

        response = {
            "status" : 200,
            "assigned_team_ids" : [10, 20, 30]            
        }
        return Response(response)

# Create your views here.
@api_view(['GET'])
def acute_care_hospital(request):
    if request.method == 'GET':
        try:
            professional_id = request.GET['professional-id']
            encounter_id = request.GET['encounter-id']
            episoode_of_care_id = request.GET['episode-of-care-id']            
        except:
            return HttpResponse(status=404)

        response = {
            "status" : 200,
        }
        return Response(response)

# Create your views here.
@api_view(['GET'])
def get_info(request):
    if request.method == 'GET':
        try:
            patient_ID = request.GET['patient-ID']
            ES_ID = request.GET['ES-ID']
            user_ID = request.GET['user-ID']
            
        except:
        	response = {
        		"status" : 404,
        		"error" : "Request error"
        	}
        	return HttpResponse(status=404)

        response = {
            "status" : 200,
            "timestamp_shift_start" : datetime.now(),
            "timestamp_shift_end" : datetime.now(),
            "emergency_section" : {
            	"ID" : ES_ID,
            	"patient_ID" : patient_ID,
            	"active" : True
            },
            "list_of_teams" : {
            	"team_ID" : 10,
            	"team_Members_ID" : [10],
            	"timestamp_invite" : datetime.now(),
            	"timestamp_treat" : datetime.now(),
            	"timestamp_revoke" : datetime.now(),
            	"timestamp_revoke_extra" : datetime.now(),

            }
        }
        return Response(response)