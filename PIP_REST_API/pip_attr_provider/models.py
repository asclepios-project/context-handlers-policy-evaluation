from django.db import models
from django.contrib.auth.models import User
#from phonenumber_field.modelfields import PhoneNumberField
import uuid
from django.contrib.auth.models import AbstractUser
#from .codes import ALLERG_INT_CODES, CONDITION_CODE, CONDITION_BODY_SITE


class User(AbstractUser):
    USER_TYPE_CHOICES = (
      (1, 'patient'),
      (2, 'ambulance'),
      (3, 'call_centre'),
      (4, 'hospital'),
      (5, 'org_admin'),
    )

    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES, null=True, default=USER_TYPE_CHOICES[0][0])

class Organization(models.Model):
    ORG_TYPE_CHOICES = (
        (1, 'Unspecified'),
        (2, 'Ambulance Service'),
        (3, 'Call Centre Service'),
        (4, 'Hospital Service'),
    )
    
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    identifier = models.IntegerField(null=True, unique=True)
    organization_active = models.BooleanField(null=True)
    organization_type = models.PositiveSmallIntegerField(choices=ORG_TYPE_CHOICES, null=True, default=ORG_TYPE_CHOICES[0][0])
    organization_name = models.CharField(null=True, max_length=100)
    organization_alias = models.CharField(null=True, max_length=100)
    organization_telecom = models.IntegerField(null=True)
    organization_zipcode = models.CharField(null=True, max_length=100)
    organization_address = models.CharField(null=True, max_length=100)
    organization_country = models.CharField(null=True, max_length=100)
    organization_city = models.CharField(null=True, max_length=100)
    
    


class Care_Team(models.Model):
    care_team_organization = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True)
    identifier = models.IntegerField(null=True, unique=True)
    care_team_name = models.CharField(null=True, max_length=100)
    care_team_tag = models.CharField(null=True, max_length=100, default="")
    care_team_is_active = models.BooleanField(null=True)
    care_team_category = models.CharField(null=True, max_length=100)
    care_team_start_datetime = models.DateTimeField(null=True)
    care_team_end_datetime = models.DateTimeField(null=True)
    care_team_reason_code = models.IntegerField(null=True)
    care_team_telecom = models.IntegerField(null=True)
    care_team_note = models.CharField(null=True, max_length=200)
    care_team_current_location = models.CharField(null=True, max_length=200)
    care_team_time_to_revoke = models.IntegerField(null=True, default=30)


class Encounter(models.Model):
    ENC_STATUS = (
        ('planned', 'Planned'),
        ('arrived', 'Arrived'),
        ('triaged', 'Triaged'),
        ('in-progress', 'In Progress'),
        ('onleave', 'On Leave'),
        ('finish', 'Finish'),
        ('cancelled', 'Cancelled'),
        ('entered-in-error', 'Entered in Error'),
        ('unknown', 'Unknown')
    )

    ENC_CLASS = (
        ('AMB', 'Ambulatory'),
        ('EMER', 'Emergency'),
        ('FLD', 'Field'),
        ('HH', 'Home health'),
        ('IMP', 'Impatient encounter'),
        ('ACUTE', 'Impatient acute'),
        ('NONAC', 'Impatient non-acute'),
        ('OBSENC', 'Observation encounter'),
        ('PRENC', 'Pre-admission'),
        ('SS', 'Short stay'),
        ('VR', 'Virtual'),
    )

    identifier = models.IntegerField(null=True, unique=True)
    encounter_patient = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    encounter_starter = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+', null=True)
    encounter_org_starter = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='+', null=True)
    encounter_org_finisher = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='++', null=True)
    encounter_identifier = models.IntegerField(null=True, unique=True)
    encounter_start_datetime = models.DateTimeField(null=True)
    encounter_end_datetime = models.DateTimeField(null=True)

# Episode of care
class Episode_Of_Care(models.Model):
    WHO_CALLED = (
        ('unknown', 'Unknown'),
        ('relative', 'Relative'),
        ('patient', 'Patient'),
        ('other', 'Other'),
    )

    SERVICE = (
      (1, 'call_centre'),
      (2, 'ambulance'),
      (3, 'hospital'),
    )

    episode_of_care_encounter = models.ForeignKey(Encounter, on_delete=models.CASCADE)
    episode_of_care_service = models.PositiveSmallIntegerField(choices=SERVICE, null=True, default=SERVICE[0][0])
    episode_of_care_team = models.ForeignKey(Care_Team, null=True, on_delete=models.CASCADE)


# Relationship who is participating in each care team
class Care_Team_Participants(models.Model):
    care_team_id = models.ForeignKey(Care_Team, on_delete=models.CASCADE, related_name="care")
    participant_id = models.ForeignKey(User, on_delete=models.CASCADE)
    participant_role = models.CharField(null=True, max_length=100)
    care_team_responsible = models.BooleanField(null=True)
    participating = models.BooleanField(null=True)
    added_date = models.DateTimeField(null=True)
